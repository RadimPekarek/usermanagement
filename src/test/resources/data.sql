INSERT INTO person (name, email, password, salary)
VALUES ('Radek Krůta', 'radek.kruta@seznam.cz', '$2a$10$FBkFi8IYnzpBlq2yTwoabO6NTixhYmlLfu5xbrJEzAhyWmKpbGz.6', 34000);
INSERT INTO person (name, email, password, salary)
VALUES ('Alfons Pampas', 'alfons.pampas@seznam.cz', '$2a$06$cjXTOlpy.GZULB/atthy3.i/QkJjwBkUxXYAsdGoIHBo9lMm9x6te',
        37000);
INSERT INTO person (name, email, password, salary)
VALUES ('Jana Olověná', 'jana.olovena@seznam.cz', '$3a$10$CKkFi8IYnzpBlq2yTwoabO6NTixhYmlLfu5xbrJEzAhyWmKpbGz.6',
        31500);
INSERT INTO person (name, email, password, salary)
VALUES ('Barbora Načatá', 'barbora.nacata@seznam.cz', '$7a$10$BKkFi8IYnzpBlq2yTwoabO6NTixhYmlLfu5xbrJEzAhyWmKpbGz.6',
        37500);
INSERT INTO person (name, email, password, salary)
VALUES ('Hana Ogrocká', 'hana.ogrocka@seznam.cz', '$2a$06$cjXTOlpy.GZULB/atthy3.i/QkJjwBkUxXYAsdGoIHBo9lMm9x6te',
        45600);
INSERT INTO person (name, email, password, salary)
VALUES ('Jana Ogrocká', 'jana.ogrocka@seznam.cz', '$2a$98$cjXTOlpy.GZULB/atthy3.i/QkJjwBkUxXYAsdGoIHBo9lMm9x6te',
        35600);

INSERT INTO group_table (name, description, expiration_date)
VALUES ('rollers', 'Přikulovači v pivovaru', '2023-09-06');
INSERT INTO group_table (name, description, expiration_date)
VALUES ('accounters', 'Skupina účetních', '2023-09-06');
INSERT INTO group_table (name, description, expiration_date)
VALUES ('sommeliers', 'Ochutnávači nápojů', '2023-09-06');
INSERT INTO group_table (name, description, expiration_date)
VALUES ('writers', 'Writing marketing texts', '2023-10-12');
INSERT INTO group_table (name, description, expiration_date)
VALUES ('teachers', 'Teaching experts group', '2023-10-12');

INSERT INTO person_group (person_id, group_id)
VALUES (1, 1);
INSERT INTO person_group (person_id, group_id)
VALUES (2, 1);
INSERT INTO person_group (person_id, group_id)
VALUES (3, 2);
INSERT INTO person_group (person_id, group_id)
VALUES (4, 2);
INSERT INTO person_group (person_id, group_id)
VALUES (1, 3);
INSERT INTO person_group (person_id, group_id)
VALUES (2, 3);
INSERT INTO person_group (person_id, group_id)
VALUES (5, 4);
INSERT INTO person_group (person_id, group_id)
VALUES (6, 4);
INSERT INTO person_group (person_id, group_id)
VALUES (6, 5);

INSERT INTO role_table (role_type, description)
VALUES ('hard manual', 'hard manual work');
INSERT INTO role_table (role_type, description)
VALUES ('soft administrative', 'soft administrative work');
INSERT INTO role_table (role_type, description)
VALUES ('cleaner', 'easy cleaning');
INSERT INTO role_table (role_type, description)
VALUES ('beer evaluation', 'tasting beers');
INSERT INTO role_table (role_type, description)
VALUES ('cheese evaluation', 'tasting cheeses');
INSERT INTO role_table (role_type, description)
VALUES ('creative', 'creating texts');
INSERT INTO role_table (role_type, description)
VALUES ('educational', 'educating people');

INSERT INTO group_role (group_id, role_id)
VALUES (1, 1);
INSERT INTO group_role (group_id, role_id)
VALUES (1, 3);
INSERT INTO group_role (group_id, role_id)
VALUES (2, 2);
INSERT INTO group_role (group_id, role_id)
VALUES (2, 3);
INSERT INTO group_role (group_id, role_id)
VALUES (3, 4);
INSERT INTO group_role (group_id, role_id)
VALUES (3, 5);
INSERT INTO group_role (group_id, role_id)
VALUES (4, 4);
INSERT INTO group_role (group_id, role_id)
VALUES (4, 5);
INSERT INTO group_role (group_id, role_id)
VALUES (5, 7);
INSERT INTO group_role (group_id, role_id)
VALUES (5, 6);

INSERT INTO address (city, country, street, postcode)
VALUES ('Brno', 'Česká republika', 'Kulatého 3', 60200);
INSERT INTO address (city, country, street, postcode)
VALUES ('Brno', 'Česká republika', 'Suchého 5', 61200);
INSERT INTO address (city, country, street, postcode)
VALUES ('Brno', 'Česká republika', 'Bernardýnova 7', 60700);
INSERT INTO address (city, country, street, postcode)
VALUES ('Brno', 'Česká republika', 'Na Střelnici 3', 60300);
INSERT INTO address (city, country, street, postcode)
VALUES ('Brno', 'Česká republika', 'Vodnářská 3', 60200);

INSERT INTO person_address (person_id, address_id)
VALUES (1, 1);
INSERT INTO person_address (person_id, address_id)
VALUES (2, 1);
INSERT INTO person_address (person_id, address_id)
VALUES (3, 2);
INSERT INTO person_address (person_id, address_id)
VALUES (4, 2);
INSERT INTO person_address (person_id, address_id)
VALUES (5, 3);
INSERT INTO person_address (person_id, address_id)
VALUES (6, 4);

