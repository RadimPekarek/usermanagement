CREATE TABLE person
(
    id       bigint       NOT NULL AUTO_INCREMENT,
    name     varchar(255) NOT NULL,
    email    varchar(100),
    password varchar(200),
    salary   bigint,
    PRIMARY KEY (id)

);
CREATE TABLE group_table
(
    id              bigint       NOT NULL AUTO_INCREMENT,
    name            varchar(500) NOT NULL,
    description     varchar(2500),
    expiration_date date,
    PRIMARY KEY (id)
);
CREATE TABLE role_table
(
    id          bigint       NOT NULL AUTO_INCREMENT,
    role_type   varchar(500) NOT NULL,
    description varchar(2500),
    PRIMARY KEY (id)
);
CREATE TABLE address
(
    id       bigint       NOT NULL AUTO_INCREMENT,
    city     varchar(500) NOT NULL,
    country  varchar(200),
    street   varchar(500),
    postcode int,
    PRIMARY KEY (id)
);

CREATE TABLE person_address
(
    person_id  bigint,
    address_id bigint,
    PRIMARY KEY (person_id, address_id),
    FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE,
    FOREIGN KEY (address_id) REFERENCES address (id) ON DELETE CASCADE
);

CREATE TABLE person_group
(
    person_id bigint,
    group_id  bigint,
    PRIMARY KEY (person_id, group_id),
    FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE,
    FOREIGN KEY (group_id) REFERENCES group_table (id) ON DELETE CASCADE
);

CREATE TABLE group_role
(
    group_id bigint,
    role_id  bigint,
    PRIMARY KEY (group_id, role_id),
    FOREIGN KEY (group_id) REFERENCES group_table (id) ON DELETE CASCADE,
    FOREIGN KEY (role_id) REFERENCES role_table (id) ON DELETE CASCADE
);


