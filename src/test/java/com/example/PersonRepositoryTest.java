package com.example;

import com.example.persistence.entities.Person;
import com.example.persistence.entities.PersonCompleteRecord;
import com.example.persistence.entities.Role;
import com.example.persistence.repository.PersonRepository;
import com.example.rest.exceptions.EntityNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserManagementApplication.class)
public class PersonRepositoryTest {
    private static Logger log = LoggerFactory.getLogger(PersonRepositoryTest.class);
    @Autowired
    PersonRepository personRepository;

    @Test
    public void getPersonById() {
        Person personBasicInfoDto = personRepository.getPersonById(1L);
        log.info("personInfo {}", personBasicInfoDto);
        assertNotNull("Should not be null", personBasicInfoDto);

    }

    @Test
    public void getPersonByEmail() {
        Person personBasicInfoDto = personRepository.getPersonByEmail("radek.kruta@seznam.cz");
        log.info("personInfo {}", personBasicInfoDto);
        assertNotNull("Should not be null", personBasicInfoDto);

    }

    @Test
    public void getPersonsRoles() throws EntityNotFoundException {
        List<Role> roles = personRepository.getPersonsRolesBasedOnId(1L);
        log.info("roles {}", roles);
        assertFalse(roles.isEmpty());
    }

    @Test
    public void getPersonFull() {
        PersonCompleteRecord personCompleteRecord = personRepository.getPersonFullById(1L);
        log.info("full {}", personCompleteRecord);
        assertNotNull(personCompleteRecord);
    }

    @Test
    public void updatePassword() {
        boolean updated = personRepository.updatePasswordById(1L, "new");
        log.info("updated {}", updated);
        PersonCompleteRecord personCompleteRecord = personRepository.getPersonFullById(1L);
        assertTrue(updated);
        assertTrue(personCompleteRecord.getPerson().getPassword().equals("new"));
    }

}
