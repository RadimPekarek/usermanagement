package com.example.service;

import com.example.persistence.dtos.*;
import com.example.persistence.entities.PersonCompleteRecord;
import com.example.persistence.entities.Role;
import com.example.persistence.repository.PersonRepository;
import com.example.rest.exceptions.EntityNotFoundException;
import com.example.utils.BeanMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PersonServiceImpl implements PersonService {
    private PersonRepository personRepository;
    private BeanMapping beanMapping;

    @Autowired
    public PersonServiceImpl(PersonRepository personRepository, BeanMapping beanMapping) {
        this.personRepository = personRepository;
        this.beanMapping = beanMapping;
    }


    @Override
    public Optional<PersonBasicInfoDto> getPersonById(Long id) {
        if (id != null)
            return Optional.ofNullable(personRepository.getPersonById(id)).map(p -> beanMapping.mapTo(p, PersonBasicInfoDto.class));
        return Optional.empty();
    }

    @Override
    public Optional<PersonBasicInfoDto> getPersonByEmail(String email) {
        if (email != null && !email.isEmpty())
            return Optional.ofNullable(personRepository.getPersonByEmail(email)).map(p -> beanMapping.mapTo(p, PersonBasicInfoDto.class));
        return Optional.empty();
    }

    @Override
    public List<RoleDto> getPersonsRolesBasedOnId(Long id) throws EntityNotFoundException {
        List<Role> roles = personRepository.getPersonsRolesBasedOnId(id);
        return beanMapping.mapTo(roles, RoleDto.class);
    }

    @Override
    @PreAuthorize("hasAuthority('MANAGER')")
    public Optional<PersonCompleteRecordDto> getPersonFullById(Long id) {
        if (id != null)
            return Optional.ofNullable(personRepository.getPersonFullById(id)).map(this::mapToPersonCompleteRecordDtoWithSortedSetsById);
        return Optional.empty();
    }

    private PersonCompleteRecordDto mapToPersonCompleteRecordDtoWithSortedSetsById(PersonCompleteRecord record) {
        if (record != null) {
            PersonCompleteRecordDto complete = new PersonCompleteRecordDto();
            complete.setPerson(beanMapping.mapTo(record.getPerson(), PersonFullInfoDto.class));
            complete.setAddress(beanMapping.mapTo(record.getAddress(), AddressDto.class));
            Set<GroupDto> converted = beanMapping.mapToSet(record.getGroups(), GroupDto.class);
            Set<GroupDto> sorted = converted.stream().sorted(Comparator.comparing(GroupDto::getId)).collect(Collectors.toCollection(LinkedHashSet::new));
            complete.setGroups(sorted);
            Set<RoleDto> convertedRoles = beanMapping.mapToSet(record.getRoles(), RoleDto.class);
            Set<RoleDto> sortedRoles = convertedRoles.stream().sorted(Comparator.comparing(RoleDto::getId)).collect(Collectors.toCollection(LinkedHashSet::new));
            complete.setRoles(sortedRoles);
            return complete;
        }
        return null;
    }
}

