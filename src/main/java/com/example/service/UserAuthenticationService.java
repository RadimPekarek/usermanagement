package com.example.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserAuthenticationService {
    private static Logger log = LoggerFactory.getLogger(UserAuthenticationService.class);
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserAuthenticationService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public boolean authenticate(String passwordFromController, UserDetails userDetails) {
        return passwordEncoder.matches(passwordFromController, userDetails.getPassword());
    }
}
