package com.example.service;

import com.example.persistence.dtos.PersonBasicInfoDto;
import com.example.persistence.dtos.PersonCompleteRecordDto;
import com.example.persistence.dtos.RoleDto;
import com.example.rest.exceptions.EntityNotFoundException;

import java.util.List;
import java.util.Optional;

public interface PersonService {
    Optional<PersonBasicInfoDto> getPersonById(Long id);

    Optional<PersonBasicInfoDto> getPersonByEmail(String email);

    List<RoleDto> getPersonsRolesBasedOnId(Long id) throws EntityNotFoundException;

    Optional<PersonCompleteRecordDto> getPersonFullById(Long id);
}
