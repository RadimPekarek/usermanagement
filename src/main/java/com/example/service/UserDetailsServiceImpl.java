package com.example.service;

import com.example.persistence.entities.Person;
import com.example.persistence.entities.Role;
import com.example.persistence.repository.PersonRepository;
import com.example.security.components.UserPrincipal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("customUserDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
    private static Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);
    private PersonRepository personRepository;

    @Autowired
    public UserDetailsServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Person person = personRepository.getPersonByEmailForAuthentication(s);
        if (person == null)
            throw new AuthenticationCredentialsNotFoundException("No person with email " + s + " found");
        List<Role> roles = personRepository.getPersonsRolesBasedOnId(person.getId());
        return new UserPrincipal(person, roles);
    }
}
