package com.example.persistence.entities;

import java.util.Objects;

public class Address {
    private Long id;
    private String city;
    private String country;
    private String street;
    private Integer postcode;

    public Address() {
    }

    public Address(String city, String country, String street, Integer postcode) {
        this.city = city;
        this.country = country;
        this.street = street;
        this.postcode = postcode;
    }

    public Address(Long id, String city, String country, String street, Integer postcode) {
        this.id = id;
        this.city = city;
        this.country = country;
        this.street = street;
        this.postcode = postcode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getPostcode() {
        return postcode;
    }

    public void setPostcode(Integer postcode) {
        this.postcode = postcode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;
        Address address = (Address) o;
        return Objects.equals(getId(), address.getId()) &&
                Objects.equals(getCity(), address.getCity()) &&
                Objects.equals(getCountry(), address.getCountry()) &&
                Objects.equals(getStreet(), address.getStreet()) &&
                Objects.equals(getPostcode(), address.getPostcode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCity(), getCountry(), getStreet(), getPostcode());
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", street='" + street + '\'' +
                ", postcode=" + postcode +
                '}';
    }
}
