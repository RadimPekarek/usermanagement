package com.example.persistence.entities;

import java.time.LocalDate;
import java.util.Objects;

public class Group {
    private Long id;
    private String name;
    private String description;
    private LocalDate expirationDate;

    public Group() {
    }

    public Group(String name, String description, LocalDate expirationDate) {
        this.name = name;
        this.description = description;
        this.expirationDate = expirationDate;
    }

    public Group(Long id, String name, String description, LocalDate expirationDate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.expirationDate = expirationDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;
        Group group = (Group) o;
        return Objects.equals(getId(), group.getId()) &&
                Objects.equals(getName(), group.getName()) &&
                Objects.equals(getDescription(), group.getDescription()) &&
                Objects.equals(getExpirationDate(), group.getExpirationDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getDescription(), getExpirationDate());
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", expiration_date=" + expirationDate +
                '}';
    }
}
