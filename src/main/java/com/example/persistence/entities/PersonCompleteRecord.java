package com.example.persistence.entities;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class PersonCompleteRecord {
    private Person person;
    private Address address;
    private Set<Group> groups = new HashSet<>();
    private Set<Role> roles = new HashSet<>();

    public PersonCompleteRecord() {
    }

    public PersonCompleteRecord(Person person, Address address) {
        this.person = person;
        this.address = address;
    }

    public PersonCompleteRecord(Person person, Address address, Set<Group> groups, Set<Role> roles) {
        this.person = person;
        this.address = address;
        this.groups = groups;
        this.roles = roles;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<Group> getGroups() {
        return groups;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersonCompleteRecord)) return false;
        PersonCompleteRecord that = (PersonCompleteRecord) o;
        return Objects.equals(getPerson(), that.getPerson()) &&
                Objects.equals(getAddress(), that.getAddress()) &&
                Objects.equals(getGroups(), that.getGroups()) &&
                Objects.equals(getRoles(), that.getRoles());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPerson(), getAddress(), getGroups(), getRoles());
    }

    @Override
    public String toString() {
        return "PersonCompleteRecord{" +
                "person=" + person +
                ", address=" + address +
                ", groups=" + groups +
                ", roles=" + roles +
                '}';
    }
}
