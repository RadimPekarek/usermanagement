package com.example.persistence.entities;

import java.util.Objects;

public class Role {
    private Long id;
    private String roleType;
    private String description;

    public Role() {
    }

    public Role(String roleType, String description) {
        this.roleType = roleType;
        this.description = description;
    }

    public Role(Long id, String role_type, String description) {
        this.id = id;
        this.roleType = role_type;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Role)) return false;
        Role role = (Role) o;
        return Objects.equals(getId(), role.getId()) &&
                Objects.equals(getRoleType(), role.getRoleType()) &&
                Objects.equals(getDescription(), role.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getRoleType(), getDescription());
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", role_type='" + roleType + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
