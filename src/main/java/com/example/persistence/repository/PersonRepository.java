package com.example.persistence.repository;

import com.example.persistence.dtos.RoleDto;
import com.example.persistence.entities.Person;
import com.example.persistence.entities.PersonCompleteRecord;
import com.example.persistence.entities.Role;
import com.example.persistence.extractors.PersonCompleteRecordResultSetExtractor;
import com.example.persistence.mappers.PersonBasicInfoMapper;
import com.example.persistence.mappers.PersonMapper;
import com.example.persistence.mappers.RoleMapper;
import com.example.rest.exceptions.RolesNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.List;


@Repository
public class PersonRepository {
    private static Logger log = LoggerFactory.getLogger(PersonRepository.class);
    @Value("${select.person.full}")
    private String personCompleteRecord;
    @Value("${select.roles}")
    private String personRoles;
    @Value("${select.by.id}")
    private String personBasicInfoById;
    @Value("${select.by.email}")
    private String personBasicInfoByEmail;
    @Value("${select.by.email.for.authentication}")
    private String personByEmailForAuthentication;
    @Value("${update.password.by.id}")
    private String updatePasswordById;
    private JdbcTemplate jdbcTemplate;


    @Autowired
    public PersonRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Person getPersonById(Long id) {
        try {
            return jdbcTemplate.queryForObject(personBasicInfoById,
                    new Object[]{id}, new PersonBasicInfoMapper());
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public Person getPersonByEmail(String email) {
        try {
            return jdbcTemplate.queryForObject(personBasicInfoByEmail,
                    new Object[]{email}, new PersonBasicInfoMapper());
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public Person getPersonByEmailForAuthentication(String email) {
        try {
            return jdbcTemplate.queryForObject(personByEmailForAuthentication,
                    new Object[]{email}, new PersonMapper());
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public List<Role> getPersonsRolesBasedOnId(Long id) {
        if (getPersonById(id) == null) throw new RolesNotFoundException(RoleDto.class, "id", id.toString());
        return jdbcTemplate.query(personRoles, new RoleMapper(), id);
    }

    public PersonCompleteRecord getPersonFullById(Long id) {
        return jdbcTemplate.query(personCompleteRecord, new PersonCompleteRecordResultSetExtractor(), id);
    }

    public boolean updatePasswordById(Long id, String password) {
        int rowsCount;
        Object[] params = {password, id};
        int[] types = {Types.VARCHAR, Types.BIGINT};
        try {
            rowsCount = jdbcTemplate.update(updatePasswordById, params, types);
        } catch (DataAccessException ex) {
            log.debug("Password not updated ", ex);
            return false;
        }
        return rowsCount > 0;
    }

}
