package com.example.persistence.extractors;

import com.example.persistence.entities.*;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class PersonCompleteRecordResultSetExtractor implements ResultSetExtractor<PersonCompleteRecord> {
    @Override
    public PersonCompleteRecord extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        PersonCompleteRecord personCompleteRecord = null;
        Set<Group> groups = new HashSet<>();
        Set<Role> roles = new HashSet<>();
        while (resultSet.next()) {
            if (personCompleteRecord == null)
                personCompleteRecord = new PersonCompleteRecord();
            if (personCompleteRecord.getPerson() == null) {
                extractPerson(resultSet, personCompleteRecord);
            }
            if (personCompleteRecord.getAddress() == null) {
                extractAddress(resultSet, personCompleteRecord);
            }
            extractGroup(resultSet, groups);
            extractRole(resultSet, roles);
            personCompleteRecord.setGroups(groups);
            personCompleteRecord.setRoles(roles);
        }

        return personCompleteRecord;
    }

    private void extractPerson(ResultSet resultSet, PersonCompleteRecord personCompleteRecord) throws SQLException, DataAccessException {
        Person person = new Person();
        person.setId(resultSet.getLong("id"));
        person.setName(resultSet.getString("name"));
        person.setEmail(resultSet.getString("email"));
        person.setPassword(resultSet.getString("password"));
        person.setSalary(resultSet.getLong("salary"));
        personCompleteRecord.setPerson(person);
    }

    private void extractAddress(ResultSet resultSet, PersonCompleteRecord personCompleteRecord) throws SQLException, DataAccessException {
        Address address = new Address();
        address.setId(resultSet.getLong("a_id"));
        address.setCity(resultSet.getString("city"));
        address.setCountry(resultSet.getString("country"));
        address.setStreet(resultSet.getString("street"));
        address.setPostcode(resultSet.getInt("postcode"));
        personCompleteRecord.setAddress(address);
    }

    private void extractGroup(ResultSet resultSet, Set<Group> groups) throws SQLException, DataAccessException {
        Group group = new Group();
        group.setId(resultSet.getLong("g_id"));
        group.setName(resultSet.getString("g_name"));
        group.setDescription(resultSet.getString("g_description"));
        group.setExpirationDate(resultSet.getDate("expiration_date").toLocalDate());
        groups.add(group);
    }

    private void extractRole(ResultSet resultSet, Set<Role> roles) throws SQLException, DataAccessException {
        Role role = new Role();
        role.setId(resultSet.getLong("r_id"));
        role.setDescription(resultSet.getString("r_description"));
        role.setRoleType(resultSet.getString("role_type"));
        roles.add(role);
    }
}
