package com.example.persistence.mappers;

import com.example.persistence.entities.Person;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class PersonBasicInfoMapper implements RowMapper<Person> {

    @Override
    public Person mapRow(ResultSet resultSet, int i) throws SQLException {
        Person person = new Person();
        person.setId(resultSet.getLong("id"));
        person.setEmail(resultSet.getString("email"));
        person.setName(resultSet.getString("name"));
        return person;
    }
}
