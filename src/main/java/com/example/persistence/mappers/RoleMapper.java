package com.example.persistence.mappers;


import com.example.persistence.entities.Role;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RoleMapper implements RowMapper<Role> {
    @Override
    public Role mapRow(ResultSet resultSet, int i) throws SQLException {
        Role role = new Role();
        role.setId(resultSet.getLong("id"));
        role.setRoleType(resultSet.getString("role_type"));
        role.setDescription(resultSet.getString("description"));
        return role;
    }
}
