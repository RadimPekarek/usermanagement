package com.example.persistence.mappers;

import com.example.persistence.entities.Person;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonMapper implements RowMapper<Person> {
    @Override
    public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
        Person person = new Person();
        person.setId(rs.getLong("id"));
        person.setEmail(rs.getString("email"));
        person.setName(rs.getString("name"));
        person.setPassword(rs.getString("password"));
        person.setSalary(rs.getLong("salary"));
        return person;
    }
}
