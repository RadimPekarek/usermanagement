package com.example.persistence.dtos;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class PersonCompleteRecordDto {
    private PersonFullInfoDto person;
    private AddressDto address;
    private Set<GroupDto> groups = new HashSet<>();
    private Set<RoleDto> roles = new HashSet<>();

    public PersonCompleteRecordDto() {
    }

    public PersonCompleteRecordDto(PersonFullInfoDto person, AddressDto address, Set<GroupDto> groups, Set<RoleDto> roles) {
        this.person = person;
        this.address = address;
        this.groups = groups;
        this.roles = roles;
    }

    public PersonFullInfoDto getPerson() {
        return person;
    }

    public void setPerson(PersonFullInfoDto person) {
        this.person = person;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public Set<GroupDto> getGroups() {
        return groups;
    }

    public void setGroups(Set<GroupDto> groups) {
        this.groups = groups;
    }

    public Set<RoleDto> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleDto> roles) {
        this.roles = roles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersonCompleteRecordDto)) return false;
        PersonCompleteRecordDto that = (PersonCompleteRecordDto) o;
        return Objects.equals(getPerson(), that.getPerson()) &&
                Objects.equals(getAddress(), that.getAddress()) &&
                Objects.equals(getGroups(), that.getGroups()) &&
                Objects.equals(getRoles(), that.getRoles());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPerson(), getAddress(), getGroups(), getRoles());
    }

    @Override
    public String toString() {
        return "PersonFull{" +
                "person=" + person +
                ", address=" + address +
                ", groups=" + groups +
                ", roles=" + roles +
                '}';
    }
}
