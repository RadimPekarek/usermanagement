package com.example.persistence.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

public class PersonFullInfoDto {
    @NotNull
    private Long id;
    @NotNull
    @NotEmpty
    private String name;
    @NotNull
    @Email
    private String email;
    @NotNull
    private String password;
    private Long salary;

    public PersonFullInfoDto() {
    }

    public PersonFullInfoDto(String name, String email, String password, Long salary) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.salary = salary;
    }

    public PersonFullInfoDto(Long id, String name, String email, String password, Long salary) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.salary = salary;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getSalary() {
        return salary;
    }

    public void setSalary(Long salary) {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersonFullInfoDto)) return false;
        PersonFullInfoDto person = (PersonFullInfoDto) o;
        return Objects.equals(getId(), person.getId()) &&
                Objects.equals(getName(), person.getName()) &&
                Objects.equals(getEmail(), person.getEmail()) &&
                Objects.equals(getPassword(), person.getPassword()) &&
                Objects.equals(getSalary(), person.getSalary());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getEmail(), getPassword(), getSalary());
    }

    @Override
    public String toString() {
        return "PersonFullInfo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", salary=" + salary +
                '}';
    }
}
