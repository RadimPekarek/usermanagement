package com.example.security.components;

public enum SecurityRole {
    USER("USER"),
    MANAGER("MANAGER");

    public final String type;

    private SecurityRole(String type) {
        this.type = type;
    }
}
