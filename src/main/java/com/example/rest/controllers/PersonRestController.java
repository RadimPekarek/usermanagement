package com.example.rest.controllers;

import com.example.persistence.dtos.PersonBasicInfoDto;
import com.example.persistence.dtos.PersonCompleteRecordDto;
import com.example.persistence.dtos.RoleDto;
import com.example.rest.exceptions.PersonNotFoundException;
import com.example.rest.exceptions.RolesNotFoundException;
import com.example.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.List;

@RestController
@Validated
@RequestMapping("/persons")
public class PersonRestController {

    private PersonService personService;

    @Autowired
    public PersonRestController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public PersonBasicInfoDto getPersonById(@PathVariable("id") @Min(1) Long id) {
        return personService.getPersonById(id).orElseThrow(() -> new PersonNotFoundException(PersonBasicInfoDto.class, "id", id.toString()));
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public PersonBasicInfoDto getPersonByEmail(@RequestParam("email") @Size(min = 5, max = 100) @Email String email) {

        return personService.getPersonByEmail(email).orElseThrow(() -> new PersonNotFoundException(PersonBasicInfoDto.class, "email", email));
    }

    @GetMapping(path = "/{id}/roles", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RoleDto> getPersonsRolesBasedOnId(@PathVariable("id") @Min(1) Long id) throws RolesNotFoundException {
        return personService.getPersonsRolesBasedOnId(id);

    }

    @GetMapping(path = "/{id}/full", produces = MediaType.APPLICATION_JSON_VALUE)
    public PersonCompleteRecordDto getPersonFullById(@PathVariable("id") @Min(1) Long id) {

        return personService.getPersonFullById(id).orElseThrow(() -> new PersonNotFoundException(PersonCompleteRecordDto.class, "id", id.toString()));
    }


}
