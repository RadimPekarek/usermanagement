package com.example.rest.exceptions.config;

import com.example.rest.exceptions.model.ApiErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebErrorConfiguration {

    @Bean
    public ErrorAttributes errorAttributes() {
        return new ApiErrorAttributes(true);
    }
}
