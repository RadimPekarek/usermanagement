package com.example.rest.exceptions.model;

import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

public class ApiErrorAttributes extends DefaultErrorAttributes {
    public ApiErrorAttributes() {
    }

    public ApiErrorAttributes(boolean includeException) {
        super(includeException);
    }

    @Override
    public Map<String, Object> getErrorAttributes(final WebRequest webRequest, final boolean includeStackTrace) {
        final Map<String, Object> defaultErrorAttributes = super.getErrorAttributes(webRequest, false);
        final ApiError apiError = ApiError.fromDefaultAttributeMap(defaultErrorAttributes);
        return apiError.toAttributeMap();
    }


}
