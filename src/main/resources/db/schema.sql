CREATE TABLE person (
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(500) NOT NULL,
  email VARCHAR(100),
	password  VARCHAR(200),
	salary BIGINT
);

CREATE TABLE group (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(500) NOT NULL,
    description VARCHAR (2500),
    expiration_date DATE
);

CREATE TABLE role (
    id BIGSERIAL PRIMARY KEY,
    role_type VARCHAR(500) NOT NULL,
    description VARCHAR (2500),
);

CREATE TABLE address (
    id BIGSERIAL PRIMARY KEY,
    city VARCHAR(500) NOT NULL,
    country VARCHAR (200),
    street VARCHAR(500),
   postcode INT
);

CREATE TABLE person_address (
  person_id BIGINT REFERENCES person(id) ON DELETE CASCADE,
  address_id BIGINT REFERENCES address(id) ON DELETE CASCADE,
  PRIMARY KEY (person_id,address_id)
);

CREATE TABLE person_group (
  person_id BIGINT REFERENCES person(id) ON DELETE CASCADE,
  group_id BIGINT,
  PRIMARY KEY (person_id,group_id),
  FOREIGN KEY (group_id) REFERENCES public.group (id) ON DELETE CASCADE
);

CREATE TABLE group_role (
  group_id BIGINT REFERENCES person(id) ON DELETE CASCADE,
  role_id BIGINT,
  PRIMARY KEY (group_id,role_id),
  FOREIGN KEY (role_id) REFERENCES public.role(id) ON DELETE CASCADE
);